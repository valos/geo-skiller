/* jshint esversion: 6 */

const MAPCOLORS7 = [ // http://www.flatuicolorpicker.com/
    '#E74C3C', // ALIZARIN : india, australia
    '#E26A6A', // SUNGLO : URSS
    '#2ECC71', // EMERALD : argentina, mongolia
    '#95A5A6', // CONCRETE : USA, chine, groenland, antartica
    '#F1C40F', // SUN FLOWER : brazil, algeria
    '#E67E22', // CARROT : canada, mexico
    '#4183D7', // ROYAL BLUE: france

    '#CAA9FA',
];

class Map {
    constructor(options) {
        this.olMap = null;
        this.options = _.defaults(options, {
            franceDepartments: false,
            places: false,
            usaStates: false,
            smallCountries: true
        });

        this.countriesVector = null;
        this.franceDepartmentsVector = null;
        this.placesVector = null;
        this.smallCountriesVector = null;
        this.usaStatesVector = null;

        this.layers = [];

        this._initMap();
    }

    clear() {
        var self = this;

        // Remove interactions
        this.olMap.getInteractions().forEach(function(interaction, i) {
            self.olMap.removeInteraction(interaction);
        });

        // Remove layers
        this.olMap.getLayers().forEach(function(layer, i) {
            self.olMap.removeLayer(layer);
        });
        this.layers = null;

        // Clear countries vector
        this.countriesVector.setLoader(function() {});
        this.countriesVector.clear({fast: true});
        this.countriesVector = null;

        // Clear places vector
        if (this.options.places) {
            this.placesVector.setLoader(function() {});
            this.placesVector.clear({fast: true});
            this.placesVector = null;
        }

        // Clear small countries vector
        if (this.options.smallCountries) {
            this.smallCountriesVector.clear({fast: true});
            this.smallCountriesVector = null;
        }

        // Clear USA states vector
        if (this.options.usaStates) {
            this.usaStatesVector.setLoader(function() {});
            this.usaStatesVector.clear({fast: true});
            this.usaStatesVector = null;
        }

        // Clear France departments vector
        if (this.options.franceDepartments) {
            this.franceDepartmentsVector.setLoader(function() {});
            this.franceDepartmentsVector.clear({fast: true});
            this.franceDepartmentsVector = null;
        }

        this.olMap.setTarget(null);
        this.olMap = null;
    }

    getCoordsDistance(firstPoint, secondPoint, projection) {
        projection = projection || 'EPSG:4326';

        var sourceProj = this.olMap.getView().getProjection();
        var c1 = ol.proj.transform(firstPoint, sourceProj, projection);
        var c2 = ol.proj.transform(secondPoint, sourceProj, projection);

        return (new ol.Sphere(6378137)).haversineDistance(c1, c2);
    }

    getCountryInfo(propertiesOrCode) {
        var properties;
        var code;
        var flag;
        var name;

        if (typeof propertiesOrCode === 'object') {
            properties = propertiesOrCode;
            code = properties.ADM0_A3;
        }
        else {
            code = propertiesOrCode;
            properties = this.countriesVector.getFeatureById(code).getProperties();
        }

        if (['England', 'Kosovo', 'Northern Cyprus', 'Northern Ireland', 'Scotland', 'Somaliland', 'Tibet', 'Wales'].includes(properties.NAME_LONG)) {
            flag = '_' + properties.NAME_LONG.replace(' ', '_');
        }
        else if (properties.ISO_A2 != -99) {
            flag = properties.ISO_A2.toLowerCase();
        }
        else if (properties.ADM0_A3 != -99) {
            flag = properties.ADM0_A3.substr(0, 2).toLowerCase();
        }

        name = i18next.t('countries:' + code);
        if (properties.TYPE === 'Dependency') {
            name += ' (' + properties.SOVEREIGNT + ')';
        }

        return {
            code: code,
            flag: flag,
            name: name,
            continent: i18next.t('countries:cont' + _.upperFirst(_.camelCase(properties.CONTINENT))),
            subregion: properties.SUBREGION !== properties.CONTINENT ? i18next.t('countries:subr' + _.upperFirst(_.camelCase(properties.SUBREGION))) : null,
            population: (properties.POP_EST / 1000000).toFixed(1)
        };
    }

    getCountryLatLngs(feature) {
        var biggest;
        var lngLats;

        if (feature.getGeometry().getType() === 'MultiPolygon') {
            lngLats = [];
            feature.getGeometry().getPolygons().forEach(function(polygon, i) {
                polygon.getCoordinates().forEach(function(subLngLats, j) {
                    lngLats.push(subLngLats);
                });
            });
        }
        else {
            lngLats = feature.getGeometry().getCoordinates();
        }

        if (lngLats.length > 1) {
            // find biggest polygon
            lngLats.forEach(function(subLngLats, i) {
                if (!biggest || subLngLats.length > biggest.length) {
                    biggest = subLngLats;
                }
            });
        }
        else {
            biggest = lngLats[0];
        }

        return biggest;
    }

    getFranceDepartmentInfo(propertiesOrCode) {
        var code;
        var properties;

        if (typeof propertiesOrCode === 'object') {
            properties = propertiesOrCode;
            code = properties.code;
        }
        else {
            code = propertiesOrCode;
            properties = this.franceDepartmentsVector.getFeatureById(code).getProperties();
        }

        return {
            code: code,
            name: properties.nom
        };
    }

    getPlaceInfo(propertiesOrCode) {
        var code;
        var properties;

        if (typeof propertiesOrCode === 'object') {
            properties = propertiesOrCode;
            code = properties.wikidataid;
        }
        else {
            code = propertiesOrCode;
            properties = this.placesVector.getFeatureById(code).getProperties();
        }

        return {
            code: code,
            country: this.getCountryInfo(properties.ADM0_A3),
            name: i18next.t('places:' + code),
            population: (properties.POP_MAX / 1000000).toFixed(1)
        };
    }

    getUsaStateInfo(propertiesOrCode) {
        var code;
        var properties;

        if (typeof propertiesOrCode === 'object') {
            properties = propertiesOrCode;
            code = properties.iso_3166_2;
        }
        else {
            code = propertiesOrCode;
            properties = this.usaStatesVector.getFeatureById(code).getProperties();
        }

        return {
            code: code,
            name: i18next.t('usa_states:' + code),
            region: i18next.t('usa_states:reg' + _.upperFirst(_.camelCase(properties.region))),
            region_sub: i18next.t('usa_states:subr' + _.upperFirst(_.camelCase(properties.region_sub)))
        };
    }

    _initCountries() {
        var self = this;

        if (this.options.smallCountries) {
            this.smallCountriesVector = new ol.source.Vector();
        }

        this.countriesVector = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            loader: function(extent, resolution, projection) {
                var format = this.getFormat();

                mapDatasets.countries.forEach(function(country, i) {
                    var feature = format.readFeature(country, {
                        featureProjection: self.olMap.getView().getProjection()
                    });
                    var fillColor;
                    var isSmall = feature.getGeometry().getArea() <= 3000000000;

                    if (self.options.usaStates || self.options.franceDepartments) {
                        fillColor = '#90a4ae';
                    }
                    else {
                        fillColor = MAPCOLORS7[country.properties.MAPCOLOR7 - 1];
                    }

                    feature.setProperties({fillColor_: fillColor, isSmall_: isSmall});
                    feature.setId(country.properties.ADM0_A3);

                    self.setCountryFeatureStyle(feature, fillColor);

                    self.countriesVector.addFeature(feature);

                    if (self.options.smallCountries && isSmall) {
                        var center = ol.extent.getCenter(feature.getGeometry().getExtent());
                        var point = feature.getGeometry().getClosestPoint(center);
                        var smallFeature = new ol.Feature(new ol.geom.Point(point));

                        smallFeature.setStyle(new ol.style.Style({
                            image: new ol.style.Icon(({
                                src: 'img/marker.png',
                                anchor: [0.5, 1]
                            }))
                        }));

                        self.smallCountriesVector.addFeature(smallFeature);
                    }
                });
            },
            strategy: ol.loadingstrategy.bbox
        });

        this.layers.push(new ol.layer.Vector({
            source: this.countriesVector,
            name: 'countries'
        }));

        if (this.options.smallCountries) {
            this.layers.push(new ol.layer.Vector({
                source: this.smallCountriesVector,
                name: 'small-countries'
            }));
        }
    }

    _initFranceDepartments() {
        var self = this;

        this.franceDepartmentsVector = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            loader: function(extent, resolution, projection) {
                var format = this.getFormat();

                mapDatasets.franceDepartments.forEach(function(state, i) {
                    var feature = format.readFeature(state, {
                        featureProjection: self.olMap.getView().getProjection()
                    });
                    var fillColor = MAPCOLORS7[6];

                    feature.setProperties({fillColor_: fillColor});
                    feature.setId(state.properties.code);

                    self.setCountryFeatureStyle(feature, fillColor);

                    self.franceDepartmentsVector.addFeature(feature);
                });
            },
            strategy: ol.loadingstrategy.bbox
        });

        this.layers.push(new ol.layer.Vector({
            source: this.franceDepartmentsVector,
            name: 'france-departments'
        }));
    }

    _initPlaces() {
        var self = this;

        this.placesVector = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            loader: function(extent, resolution, projection) {
                var format = this.getFormat();

                mapDatasets.places.forEach(function(place, i) {
                    var countryFeature = self.countriesVector.getFeatureById(place.properties.ADM0_A3);
                    var feature = format.readFeature(place, {
                        featureProjection: self.olMap.getView().getProjection()
                    });

                    if (countryFeature.getProperties().isSmall_ && place.properties.POP_MAX < 1000000) {
                        // Ignore places in small countries with population < 1M people
                        return;
                    }

                    // Switch geom: Point -> Circle
                    var geom = new ol.geom.Circle(feature.getGeometry().getCoordinates(), 100000);
                    feature.setGeometry(geom);

                    feature.setProperties({
                        CONTINENT: countryFeature.getProperties().CONTINENT
                    });
                    feature.setId(place.properties.wikidataid);

                    self.setPlaceFeatureStyle(feature);
                    self.placesVector.addFeature(feature);
                });
            },
            useSpatialIndex: false
        });

        this.layers.push(new ol.layer.Vector({
            source: this.placesVector,
            name: 'places'
        }));
    }

    _initUsaStates() {
        var self = this;

        this.usaStatesVector = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            loader: function(extent, resolution, projection) {
                var format = this.getFormat();

                mapDatasets.usaStates.forEach(function(state, i) {
                    var feature = format.readFeature(state, {
                        featureProjection: self.olMap.getView().getProjection()
                    });
                    var fillColor = MAPCOLORS7[state.properties.mapcolor9 - 1];

                    feature.setProperties({fillColor_: fillColor});
                    feature.setId(state.properties.iso_3166_2);

                    self.setCountryFeatureStyle(feature, fillColor);

                    self.usaStatesVector.addFeature(feature);
                });
            },
            strategy: ol.loadingstrategy.bbox
        });

        this.layers.push(new ol.layer.Vector({
            source: this.usaStatesVector,
            name: 'usa-states'
        }));
    }

    _initMap() {
        this._initCountries();

        if (this.options.franceDepartments) {
            this._initFranceDepartments();
        }

        if (this.options.places) {
            this._initPlaces();
        }

        if (this.options.usaStates) {
            this._initUsaStates();
        }

        this.olMap = new ol.Map({
            target: 'map',
            layers: this.layers,
            controls: [],
            interactions: [
                new ol.interaction.PinchZoom({
                    constrainResolution: false
                }),
                new ol.interaction.MouseWheelZoom(),
                new ol.interaction.DragPan()
            ],
            renderer: ol.has.WEBGL && getGlobalSettings('map_renderer_webgl').length ? 'webgl' : 'canvas',
            // Minimum distance in px the cursor must move to be detected as a map move event instead of a click
            moveTolerance: ol.has.DEVICE_PIXEL_RATIO,
            view: new ol.View({
                enableRotation: false,
                minZoom: Math.ceil(Math.max(window.outerHeight, window.outerWidth) / 640),
                maxZoom: 13
            })
        });
    }

    resetView(center, zoom) {
        var view = this.olMap.getView();

        if (!center) {
            center = [273950.37, 5557277.67];
        }
        view.animate({
            center: center,
            duration: 800,
            zoom: zoom !== undefined ? zoom : view.getMinZoom()
        });
    }

    setCountriesColors(random) {
        var self = this;
        var mapColors = MAPCOLORS7;

        if (random === true) {
            // Shuffle countries colors
            mapColors = _.shuffle(MAPCOLORS7);
        }

        this.countriesVector.getFeatures().forEach(function(feature, i) {
            var color = mapColors[feature.getProperties().MAPCOLOR7 - 1];
            feature.setProperties({fillColor_: color});
            self.setCountryFeatureStyle(feature, color);
        });

        this.countriesVector.changed();
    }

    setCountryFeatureStyle(feature, fillColor, strokeColor, strokeWidth) {
        feature.setStyle(new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: strokeColor ? strokeColor : '#eee',
                width: strokeWidth ? strokeWidth : 1
            }),
            fill: new ol.style.Fill({
                color: fillColor
            })
        }));
    }

    setPlaceFeatureStyle(feature, fillColor, strokeColor, strokeWidth) {
        feature.setStyle(new ol.style.Style({
            // opacity: 0,
            // scale: 1,
            fill: new ol.style.Fill({
                color: fillColor ? fillColor : 'rgba(255, 255, 255, 0.6)',
            }),
            stroke: new ol.style.Stroke({
                color: strokeColor ? strokeColor : 'rgba(0, 0, 255, 0.4)',
                width: strokeWidth ? strokeWidth : 1
            })
        }));
    }
}
