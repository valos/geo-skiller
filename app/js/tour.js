function Tour(el, steps) {
    var popover = null;
    var counter = 0;

    function showStep() {
        if (popover) {
            popover.close();
        }
        // var params;
        var step = steps[counter];

        if (counter === 0) {
            $$('.tour .tour-prev').addClass('color-gray');
        }
        else {
            $$('.tour .tour-prev').removeClass('color-gray');
        }
        if (counter === steps.length - 1) {
            $$('.tour .tour-next').text(__('tourOk'));
        }
        else {
            $$('.tour .tour-next').text(__('tourNext'));
        }
        $$('.tour-message', popover.el).html(step.message);

        if (_.isObject(step.target)) {
            popover.params.targetX = step.target.targetX;
            popover.params.targetY = step.target.targetY;
            delete popover.$targetEl;
            delete popover.targetEl;
        }
        else {
            popover.$targetEl = $$(step.target);
            popover.targetEl = popover.$targetEl[0];
            delete popover.params.targetX;
            delete popover.params.targetY;
        }

        popover.open();
    }

    function start() {
        if (!_.isEmpty(store.get('stats').countries) || !_.isEmpty(store.get('stats').places) || !_.isEmpty(store.get('stats').usaStates) || !_.isEmpty(store.get('stats').franceDepartments)) {
            return;
        }

        counter = 0;
        if (!popover) {
            popover = app.popover.create({
                el: el,
                closeByBackdropClick: false,
                closeByOutsideClick: false
            });
        }
        showStep();
    }

    function end() {
        popover.close();
    }

    function prev() {
        if (counter === 0) {
            return;
        }
        counter -= 1;
        showStep();
    }

    function next() {
        if (counter === steps.length - 1) {
            end();
        }
        else {
            counter += 1;
            showStep();
        }
    }

    return {
        start: start,
        prev: prev,
        next: next
    };
}
