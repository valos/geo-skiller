/* jshint esversion: 6 */

var __;
var app;
var mapDatasets = {};

// Dom7
var $$ = Dom7;

var animations = [
    'bounceInDown',
    'lightSpeedIn',
    'tada',
    'rubberBand',
    'flip',
    'rotateIn',
    'zoomInDown'
];

function getGlobalSettings(key) {
    var defaults = {
        lng: 'en',
        map_renderer_webgl: []
    };
    var settings = store.get('f7form-settings', {});

    _.defaults(settings, defaults);

    return key ? settings[key] : settings;
}

function initApp() {
    app = new Framework7({
        root: '#app',
        id: 'febvre.info.geoskiller', // App bundle ID
        name: 'Geo Skiller',
        theme: 'auto',
        view: {
            animateWithJS: true
        },
        // App root data
        data: function () {
            return {};
        },
        dialog: {
            buttonCancel: __('dialogButtonCancel'),
        },
        // App root methods
        methods: {
            hideSplashMessage: function() {
                $$('#splash-message span').removeAttr('class').empty();
                $$('#splash-message').css('display', 'none');
            },

            showNotification: function(title, subtitle, text) {
                var date = new Date();
                var notif = app.notification.create({
                    icon: '<i class="icon material-icons">update</i>',
                    title: title,
                    titleRightText: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(undefined, {hour12: false}),
                    subtitle: subtitle,
                    text: text,
                    closeOnClick: true
                });

                notif.open();
            },

            showSplashMessage: function(message, animation, animationendCb) {
                var self = this;

                if (!animation) {
                    animation = animations[Math.floor(Math.random() * animations.length)];
                }

                $$('#splash-message span').html(message);
                $$('#splash-message').css('display', 'table');
                if (animation === 'none') {
                    return;
                }

                $$('#splash-message span')
                    .removeAttr('class')
                    .addClass(animation + ' animated')
                    .once('animationend', function(e) {
                        window.setTimeout(function () {
                            self.hideSplashMessage();
                            if (animationendCb) {
                                animationendCb();
                            }
                        }, 600);
                    });
            }
        },
        on: {
            init: function() {
                $$('.panel-left a[href="/settings/"] .item-title').html(__('settingsTitle'));
                $$('.panel-left a[href="/about/"] .item-title').html(__('aboutTitle'));
            },
            formStoreData: function(form, data) {
                i18next.changeLanguage(data.lng);
            },
            panelClosed: function(panel) {
                if (panel.side === 'right') {
                    // A page is used to display Settings in right panel. We use push state.
                    // When panel backdrop is clicked, panel is closed.
                    // Settings page must be closed in panel view 
                    app.views.get('.panel-right-view').router.back();
                }
            }
        },
        routes: routes,
        panel: {
            leftBreakpoint: 4096, // panel left visibility breakpoint,
            swipe: 'both',
            swipeOnlyClose: true
        },
    });

    // Create main view and panel right view
    app.views.create('.view-main', {
        url: '/',
        main: true,
        pushState: true,
        pushStateRoot: location.href.split(location.host)[1].split('#!')[0]
    });

    app.views.create('.panel-right-view', {
        pushState: true,
        pushStateRoot: location.href.split(location.host)[1].split('#!')[0]
    });

    window.onpopstate = function(e) {
        // A page is used to display Settings in right panel. We use push state.
        // When back button is used, page is closed (history back) but panel not
        if (app.panel.right.opened) {
            app.panel.close('right');
        }

        // Close all opened popovers too
        app.popover.close();
    };
}

function initDatasets() {
    var counter = 0;
    var countriesCodes;
    var datasets = ['countries', 'places', 'usa_states', 'france_departments']; // countries MUST BE first

    datasets.forEach(function(name, i) {
        // http://geojson.xyz/
        // http://geojson.io/
        var req = new Request('data/' + name + '.geojson');

        fetch(req)
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                mapDatasets[_.camelCase(name)] = data.features;

                if (name === 'countries') {
                    countriesCodes = _.map(data.features, function(f) {
                        return f.properties.ADM0_A3;
                    });
                }
                counter++;

                if (counter === datasets.length) {
                    // End of datasets

                    // Filter places with unknown country
                    mapDatasets.places = _.filter(mapDatasets.places, function(f) {
                        return _.includes(countriesCodes, f.properties.ADM0_A3);
                    });

                    initApp();
                }
            });
    });
}

function initI18n() {
    Template7.registerHelper('__', function(key, options) {
        return i18next.t(key, options.hash);
    });

    i18next
        .use(i18nextXHRBackend)
        .use(i18nextBrowserLanguageDetector)
        .init({
            detection: {
                order: ['localStorage', 'navigator'],
            },
            fallbackLng: 'en',
            whitelist: ['en', 'fr'],
            load: 'languageOnly',
            debug: false,
            ns: ['translation', 'countries', 'places', 'usa_states'],
            defaultNS: 'translation',
            backend: {
                loadPath: 'locales/{{lng}}/{{ns}}.json'
            }
        }, function(err, t) {
            initDatasets();
        });

        __ = function(key, options) {
            return i18next.t(key, options);
        };
}

function start() {
    // Service Worker registration
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('sw.js').then(registration => {

            // updatefound is fired if sw.js changes
            registration.onupdatefound = function() {
                // The updatefound event implies that registration.installing is set; see
                // https://w3c.github.io/ServiceWorker/#service-worker-registration-updatefound-event
                var installingWorker = registration.installing;

                installingWorker.onstatechange = function() {
                    if (installingWorker.state == 'installed' && navigator.serviceWorker.controller) {
                        // At this point, the old content will have been purged and the fresh content will
                        // have been added to the cache.
                        // It's the perfect time to display a message in the page's interface.
                        app.methods.showNotification(
                            __('swUpdateNotificationTitle'),
                            __('swUpdateNotificationSubtitle'),
                            __('swUpdateNotificationText')
                        );
                    }
                };
            };
        }).catch(registrationError => {
            console.log('SW registration failed: ', registrationError);
        });
    }

    // Init storage
    if (!store.get('stats')) {
        store.set('stats', {});
    }
    store.transact('stats', function(stats) {
        if (!stats.countries) {
            stats.countries = {};
        }
        if (!stats.places) {
            stats.places = {};
        }
        if (!stats.franceDepartments) {
            stats.franceDepartments = {};
        }
        if (!stats.usaStates) {
            stats.usaStates = {};
        }

        return stats;
    });

    initI18n();
}

if (window.location.hash !== '') {
    window.location.href = '';
}
else {
    // Request user for persistent storage
    if (navigator.storage && navigator.storage.persist) {
        navigator.storage.persist().then(function(persistent) {
            start();
        });
    }
    else {
        start();
    }
}
