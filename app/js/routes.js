routes = [
    {
        path: '/',
        componentUrl: './pages/home.html'
    },
    {
        path: '/about/',
        templateUrl: './pages/about.html',
        options: {
            context: {
                map: ol.has.WEBGL ? 'webgl' : 'canvas'
            }
        }
    },
    {
        path: '/settings/',
        componentUrl: './pages/settings.html',
    },
    {
        path: '/countries/',
        component: './pages/countries.html',
        routes: [
            {
                path: 'settings/',
                componentUrl: './pages/countries-settings.html'
            },
            {
                path: 'stats/',
                popup: {
                    templateUrl: './pages/countries-stats.html'
                }
            }
        ]
    },
    {
        path: '/places/',
        component: './pages/places.html',
        routes: [
            {
                path: 'settings/',
                componentUrl: './pages/places-settings.html'
            },
            {
                path: 'stats/',
                popup: {
                    templateUrl: './pages/places-stats.html'
                }
            }
        ]
    },
    {
        path: '/usa-states/',
        component: './pages/usa-states.html',
        routes: [
            {
                path: 'settings/',
                componentUrl: './pages/usa-states-settings.html'
            },
            {
                path: 'stats/',
                popup: {
                    templateUrl: './pages/usa-states-stats.html'
                }
            }
        ]
    },
    {
        path: '/france-departments/',
        component: './pages/france-departments.html',
        routes: [
            {
                path: 'settings/',
                componentUrl: './pages/france-departments-settings.html'
            },
            {
                path: 'stats/',
                popup: {
                    templateUrl: './pages/france-departments-stats.html'
                }
            }
        ]
    },
    // Default route (404 page). MUST BE THE LAST
    {
        path: '(.*)',
        url: './pages/404.html',
    }
];
