#! /usr/bin/env python

from collections import OrderedDict
import io
import json
import re
word_regex_pattern = re.compile("[^A-Za-z]+")

def camel(chars):
    words = word_regex_pattern.split(chars)
    return ''.join(w.title() for i, w in enumerate(words))

o = "app/locales/en/countries.json"
of = io.open(o, 'w', encoding="utf-8")

data = OrderedDict()
data_continents = dict()
data_subregions = dict()
with open('app/data/countries.geojson', 'r') as fp:
    countries = json.load(fp)['features']

    for country in countries:
        code = country['properties']['GU_A3']
        name = country['properties']['NAME_LONG']
        data[code] = name

        continent = country['properties']['CONTINENT']
        if continent not in data_continents.values():
            data_continents['cont{}'.format(camel(continent))] = continent
        subregion = country['properties']['SUBREGION']
        if subregion not in data_subregions.values():
            data_subregions['subr{}'.format(camel(subregion))] = subregion

    for code, name in data_continents.items():
        data[code] = name
    for code, name in data_subregions.items():
        data[code] = name

    data = json.dumps(data, ensure_ascii=False, separators=(',', ': '), indent=0)
    of.write(data)

of.close()
