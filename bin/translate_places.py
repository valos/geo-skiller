#! /usr/bin/env python

from collections import OrderedDict
import io
import json

o = "app/locales/en/places.json"
of = io.open(o, 'w', encoding="utf-8")

data = OrderedDict()
with open('app/data/places.geojson', 'r') as fp:
    places = json.load(fp)['features']

    for place in places:
        code = place['properties']['wikidataid'] # GEONAMEID, UN_FID, wof_id
        name = place['properties']['NAME']
        data[code] = name

    data = json.dumps(data, ensure_ascii=False, separators=(',', ': '), indent=0)
    of.write(data)

of.close()
