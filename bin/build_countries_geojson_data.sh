#! /bin/sh

scriptdir=$(dirname $(readlink -f $(which $0)))

cd $scriptdir

if [ ! -d "../app/data" ]; then
    mkdir ../app/data
fi
cd ../app/data

rm *.geojson

#
# Countries
#

FILE=ne_50m_admin_0_countries
# FILE=ne_50m_admin_0_countries_lakes
# FILE=ne_110m_admin_0_countries
# FILE=ne_110m_admin_0_map_units
URL=http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/cultural/$FILE.zip
OUT=countries.geojson

wget $URL

mkdir tmp
unzip $FILE.zip -d tmp

../../node_modules/mapshaper/bin/mapshaper tmp/$FILE.shp -simplify 15% planar keep-shapes -o format=geojson precision=0.01 $OUT

rm -Rf tmp $FILE.zip

#
# Places
#

FILE=ne_110m_populated_places
# FILE=ne_110m_populated_places_simple
URL=http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/110m/cultural/$FILE.zip
OUT=places.geojson

wget $URL

mkdir tmp
unzip $FILE.zip -d tmp

../../node_modules/mapshaper/bin/mapshaper tmp/$FILE.shp -o format=geojson precision=0.001 $OUT

rm -Rf tmp $FILE.zip

#
# United States: States, provinces
#

FILE=ne_110m_admin_1_states_provinces

URL=http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/110m/cultural/$FILE.zip
OUT=usa_states.geojson

wget $URL

mkdir tmp
unzip $FILE.zip -d tmp

../../node_modules/mapshaper/bin/mapshaper tmp/$FILE.shp -o format=geojson precision=0.01 $OUT

rm -Rf tmp $FILE.zip

#
# France departments
#

FILE=departements-avec-outre-mer

URL=https://raw.githubusercontent.com/gregoiredavid/france-geojson/v2-hd-dev/$FILE.geojson
OUT=france_departments.geojson

wget $URL

../../node_modules/mapshaper/bin/mapshaper $FILE.geojson -simplify 5% planar keep-shapes -o precision=0.01 $OUT
rm $FILE.geojson
