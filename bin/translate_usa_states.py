#! /usr/bin/env python

from collections import OrderedDict
import io
import json
import re
word_regex_pattern = re.compile("[^A-Za-z]+")

def camel(chars):
    words = word_regex_pattern.split(chars)
    return ''.join(w.title() for i, w in enumerate(words))

o = "app/locales/en/usa_states.json"
of = io.open(o, 'w', encoding="utf-8")

data = OrderedDict()
data_regions = dict()
data_subregions = dict()
with open('app/data/usa_states.geojson', 'r') as fp:
    states = json.load(fp)['features']

    for state in states:
        code = state['properties']['iso_3166_2']
        name = state['properties']['name']
        data[code] = name

        region = state['properties']['region']
        if region not in data_regions.values():
            data_regions['reg{}'.format(camel(region))] = region
        subregion = state['properties']['region_sub']
        if subregion not in data_subregions.values():
            data_subregions['subr{}'.format(camel(subregion))] = subregion

    for code, name in data_regions.items():
        data[code] = name
    for code, name in data_subregions.items():
        data[code] = name

    data = json.dumps(data, ensure_ascii=False, separators=(',', ': '), indent=0)
    of.write(data)

of.close()
