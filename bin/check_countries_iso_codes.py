#! /usr/bin/env python

import csv
import json

o = "check_countries_iso_codes.csv"
of = open(o, "w+")

cols = ['NAME', 'NAME_LONG', 'ABBREV', 'ADM0_A3', 'BRK_A3', 'FIPS_10_', 'GU_A3', 'ISO_A2', 'ISO_A3', 'POSTAL', 'SOV_A3', 'WB_A2', 'WB_A3']

with open('./app/data/countries.geojson', 'r') as fp:
    countries = json.load(fp)['features']

    writer = csv.writer(of)
    writer.writerow(cols)

    for country in countries:
        row = []
        for col in cols:
            row.append(country['properties'][col].encode('utf-8'))

        writer.writerow(row)
