#! /bin/sh

scriptdir=$(dirname $(readlink -f $(which $0)))

cd $scriptdir

URL=https://raw.githubusercontent.com/nvkelso/natural-earth-vector/master/geojson
FILE=ne_110m_admin_0_countries.geojson
# FILE=ne_110m_admin_0_map_units.geojson

if [ ! -d "../app/data" ]; then
    mkdir ../app/data
fi

wget -O ../app/data/countries.geojson $URL/$FILE
