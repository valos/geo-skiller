# Geo Skiller

Geography app (PWA) to learn world countries in a fun way

## Usage

### 1. Download this repository
```
git clone https://gitlab.com/valos/geo-skiller
```

### 2. Install Yarn

As root user, run:
```
npm install yarn -g
```

### 3. Install dependencies

Go to the downloaded repository folder and run:
```
yarn install
```

### 4. Download and build countries data (GeoJSON with countries properties)

```
./bin/build_countries_geojson_data.sh
```

Data are provided by [Natural Earth](http://www.naturalearthdata.com/).

### 5. Generate service worker

```
gulp generate-service-worker
```

### 6. Run the app

```
yarn run serve
```

App will be opened in browser at `http://localhost:8080/`

## One command install

```
git clone https://gitlab.com/valos/geo-skiller &&
cd geo-skiller &&
yarn install &&
./bin/build_countries_geojson_data.sh &&
gulp generate-service-worker &&
yarn run serve
```

## Live Preview

https://geoskiller.febvre.info


## Copyright

&copy; 2015-2018 Valéry Febvre, All Rights Reserved
