"use strict";
/* jshint esversion: 6 */
/* jshint node: true */

const gulp = require('gulp');

const workbox = require('workbox-build');

const dist = 'app';

gulp.task('build', () => {
});

gulp.task('clean', () => {
});

gulp.task('generate-service-worker', () => {
    return workbox.generateSW({
        cacheId: 'geo-skiller',
        globDirectory: dist,
        globPatterns: ['**\/*.{css,geojson,html,ico,js,json,png,woff2}'],
        swDest: `${dist}/sw.js`,
        importScripts: [],
        importWorkboxFrom: 'local',
        clientsClaim: true,
        skipWaiting: true
    }).then(() => {
        console.info('Service worker generation completed.');
    }).catch((error) => {
        console.warn('Service worker generation failed: ' + error);
    });
});

const sort = require('gulp-sort');
const i18next = require('i18next-scanner');
const fs = require('fs');
const _ = require('lodash');

gulp.task('i18next', function() {
    return gulp.src(['app/pages/*.html', 'app/js/*', 'app/index.html'])
        .pipe(sort())
        .pipe(i18next({
            lngs: ['en', 'fr'],
            attr: false,
            func: {
                list: ['__'],
                extensions: ['.js', '.html']
            },
            trans: false,
            removeUnusedKeys: true,
            resource: {
                loadPath: 'app/locales/{{lng}}/{{ns}}.json',
                savePath: '{{lng}}/{{ns}}.json'
            },
            sort: true
        },
        function(file, enc, done) {
            var parser = this.parser;
            var content = fs.readFileSync(file.path, enc);

            // Template7 __ function helper
            (function() {
                var results = content.match(/{{__\s+("(?:[^"\\]|\\.)*"|'(?:[^'\\]|\\.)*')?([^}]*)}}/gm) || [];
                _.each(results, function(result) {
                    var value;
                    var r = result.match(/{{__\s+("(?:[^"\\]|\\.)*"|'(?:[^'\\]|\\.)*')?([^}]*)}}/m) || [];

                    if (!_.isUndefined(r[1])) {
                        value = _.trim(r[1], '\'"');
                        parser.set(value, '');
                    }
                });
            }());

            done();
        }))
        .pipe(gulp.dest('app/locales'));
});

const runSequence = require('run-sequence');

gulp.task('default', () => {
    runSequence('i18next', 'generate-service-worker');
});
